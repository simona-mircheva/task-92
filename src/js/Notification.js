// Add imports 
import classNames from "classnames";
import {formatCurrency} from './utils';
export default class Notification {
  static get types() {
    return {
      PEPPERONI: "pepperoni",
      MARGHERITA: "margherita",
      HAWAIIAN: "hawaiian",
    };
  }

  constructor() {
    this.container = document.createElement("div");
    this.container.classList.add("notification-container");

    if (this._type === Notification.types.HAWAIIAN) {
      this.container.classList.add("is-danger");
    }
  }
  empty() {
    this.container.remove();
  }

  render({price, type}) {
    
    const template = `
      <div class=""notification type-${type} ${classNames({"is-danger": type === Notification.types.HAWAIIAN},)}">
        <button class="delete"></button>
        🍕 <span class="type">${type}</span> (<span class="price">${formatCurrency(price)}</span>) has been added to your order.
        </div>
    `;

    this.container.innerHTML = template;

    const deleteButton = this.container.querySelector(".delete");
    deleteButton.onclick = () => this.empty();
    }
  }